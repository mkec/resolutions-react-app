import React, { Component } from 'react';
import { connect } from 'react-redux';

import Input from '../../components/UI/Form/Input/Input';
import Button from '../../components/UI/Form/Button/Button';
import classes from './Auth.module.css';
import * as actions from '../../store/actions/auth';

class PasswordReset extends Component {
    state = {
        controls: {
            email: {
                elementType: 'input',
                elementConfig: {
                    type: 'email',
                    placeholder: 'Mail Address'
                },
                value: '',
                validation: {
                    required: true,
                    isEmail: true
                },
                valid: false,
                touched: false
            },
            password: {
                elementType: 'input',
                elementConfig: {
                    type: 'password',
                    placeholder: 'Password'
                },
                value: '',
                validation: {
                    required: true,
                    minLength: 8
                },
                valid: false,
                touched: false
            },
            password_confirmation: {
                elementType: 'input',
                elementConfig: {
                    type: 'password',
                    placeholder: 'Confirm Password'
                },
                value: '',
                validation: {
                    required: true,
                    minLength: 8
                },
                valid: false,
                touched: false
            }
        },
        token: null,
        formIsValid: false
    }

    checkValidity(value, rules) {
        let isValid = true;
        if (!rules) {
            return true;
        }

        if (rules.required) {
            isValid = value.trim() !== '' && isValid;
        }

        if (rules.minLength) {
            isValid = value.length >= rules.minLength && isValid
        }

        if (rules.maxLength) {
            isValid = value.length <= rules.maxLength && isValid
        }

        if (rules.isEmail) {
            const pattern = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
            isValid = pattern.test(value) && isValid
        }

        if (rules.isNumeric) {
            const pattern = /^\d+$/;
            isValid = pattern.test(value) && isValid
        }

        return isValid;
    }

    inputChangedHandler = (event, controlName) => {
        let updatedControls = null;
        if (controlName === 'password_confirmation') {
            updatedControls = {
                ...this.state.controls,
                [controlName]: {
                    ...this.state.controls[controlName],
                    value: event.target.value,
                    valid: (this.checkValidity(event.target.value, this.state.controls[controlName].validation) && (this.state.controls.password.value === event.target.value)),
                    touched: true
                }
            };
        } else if (controlName === 'password') {
            updatedControls = {
                ...this.state.controls,
                [controlName]: {
                    ...this.state.controls[controlName],
                    value: event.target.value,
                    valid: this.checkValidity(event.target.value, this.state.controls[controlName].validation),
                    touched: true
                },
                'password_confirmation': {
                    ...this.state.controls.password_confirmation,
                    valid: (!this.state.controls.password_confirmation.touched ||
                        (this.state.controls.password_confirmation.value !== '' && this.state.controls.password_confirmation.value === event.target.value)),
                    touched: this.state.controls.password_confirmation.touched
                }
            };
        } else {
            updatedControls = {
                ...this.state.controls,
                [controlName]: {
                    ...this.state.controls[controlName],
                    value: event.target.value,
                    valid: this.checkValidity(event.target.value, this.state.controls[controlName].validation),
                    touched: true
                }
            };
        }
        let formIsValid = true;
        for (let inputIdentifier in updatedControls) {
            formIsValid = updatedControls[inputIdentifier].valid && formIsValid;
        }
        this.setState({ controls: updatedControls, formIsValid: formIsValid });
    }

    componentDidMount() {
        const token = this.props.match.params.token;
        let params = new URLSearchParams(this.props.location.search);
        let email = params.get('email');
        if (email) {
            const updatedControls = {
                ...this.state.controls,
                email: {
                    ...this.state.controls.email,
                    value: email,
                    valid: this.checkValidity(email, this.state.controls.email.validation),
                    touched: true
                }
            };
            this.setState({ token: token, controls: updatedControls });
        }
    }

    submitHandler = (event) => {
        event.preventDefault();
        if (!this.state.token) {
            this.props.onResetInit(this.state.controls.email.value);
        } else {
            this.props.onReset(this.state.controls.email.value, this.state.token, this.state.controls.password.value, this.state.controls.password_confirmation.value);
        }
    }

    render() {
        const formElementsArray = [];
        for (let key in this.state.controls) {
            formElementsArray.push({
                id: key,
                config: this.state.controls[key]
            });
        }

        const form = formElementsArray.map(formElement => (
            <Input
                className={this.state.token ? classes.Visible : ((formElement.id === 'password' || formElement.id === 'password_confirmation') ? classes.Hidden : classes.Visible)}
                key={formElement.id}
                elementType={formElement.config.elementType}
                value={formElement.config.value}
                placeholder={formElement.config.elementConfig.placeholder}
                type={formElement.config.elementConfig.type}
                invalid={!formElement.config.valid}
                shouldValidate={formElement.config.validation}
                touched={formElement.config.touched}
                changed={(event) => this.inputChangedHandler(event, formElement.id)} />
        ));

        let errorMsgs = [];
        let messagesArr = [];
        console.log(this.props.err);
        if (this.props.err) {
            if (this.props.err.data.message)
                messagesArr = <span style={{ textAlign: "left", color: "red", paddingBottom: "10px", display: "block" }}>{this.props.err.data.message}</span>;
        }
        if (this.props.err && !this.props.err.data.message) {
            let errs = this.props.err.data.errors;
            for (let x in errs) {
                errorMsgs.push(errs[x]);
            }
            messagesArr = errorMsgs.map(item => <span style={{ textAlign: "left", color: "red", paddingBottom: "10px", display: "block" }}>{item}</span>)
        }
        let authLabel = this.state.token ? 'Update password' : 'Password Reset';
        let infoMsg = this.props.msg? <div style={{ textAlign: "left", color: "green", paddingTop: "20px", display: "block"}}>{this.props.msg}</div> : null;
        return (

            <React.Fragment>
                <div className={classes.Auth}>
                    <div style={{ textAlign: "left", padding: "0px", margin: "0px", height: "15px", fontSize: "1.2em" }}>{authLabel}</div>
                    {infoMsg}
                    <form onSubmit={this.submitHandler}>
                        {form}
                        {messagesArr}
                        <Button clicked={this.submitHandler} btnType="Success" disabled={!this.state.formIsValid}>SUBMIT</Button>
                    </form>
                </div>
            </React.Fragment>
        );
    }
}

// REDUX
const mapStateToProps = state => {
    return {
        err: state.ardcr.error,
        msg: state.ardcr.resetMessage
    }
}
const mapDispatchToProps = dispatch => {
    return {
        onResetInit: (email) => dispatch(actions.resetInit(email)),
        onReset: (email, token, password, confirm_password) => dispatch(actions.reset(email, token, password, confirm_password)),
    };
};
export default connect(mapStateToProps, mapDispatchToProps)(PasswordReset);