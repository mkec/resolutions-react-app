import React, { Component } from 'react';
import { connect } from 'react-redux';
import Loader from 'react-loader';

import Pagination from '../../components/Pagination/Pagination';
import * as actions from '../../store/actions/actions';
import ResolutionItem from '../../components/UI/ResolutionItem/ResolutionItem';
import classes from './Content.module.css';
import * as actionTypes from '../../store/actions/actionTypes';
/**
 * @description Item content container
 *
 * @class Content
 * @extends {Component}
 */
class Content extends Component {
    constructor(props) {
        super(props)
        this.props.onDataGet(this.props.datep, this.props.ppg, 1, this.props.api_token ? this.props.email : null);
    }
    componentDidMount() {
        window.scrollTo(0, 0)
    }
    /**
     * @description Setting and filtering date format
     * @summary Prepare date param for ajax query
     * @param {*} newProps
     * @param {*} newState
     * @memberof Content
     */
    UNSAFE_componentWillUpdate(newProps, newState) {
        if ((newProps.datep !== this.props.datep || newProps.api_token !== this.props.api_token) && this.props.datep !== null) {
            this.props.onDataGet(newProps.datep, newProps.ppg, 1, newProps.api_token ? newProps.email : null);
        }
    }
    /**
     * @description call ajax if/when date changes
     * @param {*} newProps
     * @param {*} newState
     * @returns
     * @memberof Content
     */
    shouldComponentUpdate(newProps, newState) {
        if ((newProps.datep !== this.props.datep || newProps.api_token !== this.props.api_token) && this.props.datep !== null) {
            this.props.onDataGet(newProps.datep, newProps.ppg, 1, newProps.api_token ? newProps.email : null);
        }
        if (newProps.tmsht !== this.props.tmsht) {
            return true;
        }
        return false;
    }

    onChangePage(currentPage) {
        if (parseInt(currentPage) < 1 || parseInt(currentPage) > parseInt(this.props.lp)) return
        //update new current page
        this.props.onPageChange(currentPage);
        this.props.onDataGet(this.props.datep, this.props.ppg, currentPage, this.props.api_token ? this.props.email : null);
    }

    render() {
        let items = null;
        if (this.props.tmsht.length > 0) {
            let tmmshts = this.props.tmsht;
            items = (
                tmmshts.map((elem, k) => {
                    if (this.props.api_token) {
                        return <ResolutionItem edit={true} startdate={elem.start} reminder={elem.reminder} status={elem.status} untildate={elem.until} key={Math.floor(Math.random() * 10000)} id={elem.id} title={elem.title} body={elem.body} clicked={(event) => this.props.onModalOpen(event)} />
                    } else {
                        return <ResolutionItem edit={true} startdate={elem.start} reminder={elem.reminder} status={elem.status} untildate={elem.until} key={Math.floor(Math.random() * 10000)} id={elem.id} title={elem.title} body={elem.body} clicked={() => this.props.onModalLoginOpen()} />
                    }
                })
            );
        }

        let activateLoader = this.props.ttlCnt===null? false : true;
        let final_content = (< main className={classes.Main} >
            <div className="wrap">
                <div style={{ minHeight: "200px"}}>
                    <Loader
                        loaded={{loader:activateLoader}}
                        lines={13}
                        length={20}
                        width={10}
                        radius={30}
                        corners={1}
                        rotate={0}
                        direction={1}
                        color="#000"
                        speed={1}
                        trail={60}
                        shadow={false}
                        hwaccel={false}
                        className="spinner"
                        zIndex={2e9}
                        top="50%"
                        left="50%"
                        scale={1.0}
                        loadedClassName="loadedContent"
                    />
                </div>
            </div>
        </ main>);
        if (this.props.tmsht.length > 0) {
            final_content = (< main className={classes.Main} >
                <div className="wrap">
                    {items}
                </div>
                <div className="wrap">
                    <div className="container">
                        <div className="row justify-content-center">
                            <div className="text-center">
                                <Pagination
                                    paginationButtons={this.props.pbuttons}
                                    currentPage={this.props.cp}
                                    totalPages={this.props.ttlCnt}
                                    lastPage={this.props.lp}
                                    setPage={this.onChangePage.bind(this)}
                                />
                            </div>
                        </div>
                    </div>
                </div>
            </main >);
        }
        return (final_content);
    }
}

// REDUX
const mapStateToProps = state => {
    return {
        tmsht: state.rdcr.timesheet,//items from specific date
        slct: state.rdcr.selectedItem,//selected item from container
        datep: state.rdcr.dateParam,// date from url
        ttlCnt: state.rdcr.totalItemsCount,//from api
        ppg: state.rdcr.perPage,//items per page
        cp: state.rdcr.currPage,//current page
        lp: state.rdcr.lastPage,//last page
        pbuttons: state.rdcr.paginationButtons,
        api_token: state.ardcr.token,
        email: state.ardcr.email
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onPageChange: (currentPage) => dispatch({ type: actionTypes.PAGE_CHANGE, current: currentPage }),
        onModalLoginOpen: () => dispatch({ type: actionTypes.OPEN_LOGIN_MODAL, modalTitle: "Authentication", selectedItem: null }),
        onModalOpen: (event) => dispatch({ type: actionTypes.OPEN_MODAL, modalTitle: "Update/Delete resolution", selectedItem: event }),
        onDataGet: (date, perPage, currPage, email) => dispatch(actions.onDataGet(date, perPage, currPage, email))
    };
}
export default connect(mapStateToProps, mapDispatchToProps)(Content);