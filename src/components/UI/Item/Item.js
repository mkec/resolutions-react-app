import React, { Component } from 'react';
import classes from './Item.module.css';

import ReadOnly from '../../SlateEditor/SlateReadOnly';
//import SlateEditor from '../../SlateEditor/SlateEditor';
import 'typeface-roboto';
/**
 * @description Timesheet display item component
 * @class Item
 * @extends {Component}
 */
class Item extends Component {
    render() {
        return (
            /* <div id={this.props.id} className={classes.ItemRow} onClick={() =>this.props.clicked(this.props.id)}>
                <div className={classes.CheckFlag}>
                    <span className={classes.SmallTextLabel }>Title</span>
                    <span className={classes.SmallTextLabel + ' ' + classes.Hours}>Reminder</span>
                    <span className={classes.CheckFlagLabel}>{this.props.title}</span>
                    <span className={classes.HoursBox}>{this.props.reminder}</span>
                </div>
            </div> */
            <div id={this.props.id} className={classes.ItemRow + " card"} onClick={() => this.props.clicked(this.props.id)}>
                <h5 className="card-header">{this.props.title}</h5>
                <div className="card-body">
                    <h5 className="card-title">Special title treatment</h5>
                    <ReadOnly
                        key={this.props.id}
                        body={this.props.body}
                    />
                </div>
            </div >
        );
    }
}

export default Item;